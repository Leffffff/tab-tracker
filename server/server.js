const http = require("http");
const url = require('url');
const { readFileSync, existsSync } = require("fs");
const { extname } = require('path')
const { StringDecoder } = require('string_decoder')

const host = 'localhost';
const port = 8000;

var mimeTypes = {
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.css': 'text/css',
    '.json': 'application/json',
    '.png': 'image/png',
    '.jpg': 'image/jpg',
    '.gif': 'image/gif',
    '.wav': 'audio/wav',
    '.mp4': 'video/mp4',
    '.woff': 'application/font-woff',
    '.ttf': 'application/font-ttf',
    '.eot': 'application/vnd.ms-fontobject',
    '.otf': 'application/font-otf',
    '.svg': 'application/image/svg+xml'
};

const requestListener = function (req, res) {

    const parsedURL = url.parse(req.url, true);
    const path = parsedURL.pathname;
    const trimmedPath = path.replace(/^\/+|\/+$/g, '');
    const queryStringObj = parsedURL.query;

    const decoder = new StringDecoder('utf-8');

    console.log('INFO: requestListener -> path', path);
    switch (path) {
        case "/browse":
            res.writeHead(200);
            res.end(readFileSync('./browse.html'));
            break;
        case "/favorite":
            res.writeHead(200);
            res.end(readFileSync('./favorite.html'));
            break;
        case "/history":
            res.writeHead(200);
            res.end(readFileSync('./history.html'));
            break;

        // api
        case "/search": // ?q={string}
            res.writeHead(200);
            res.end({
                // q of songs matched string
            });
            break;
        case "/list": // ?offset={number}&max_size={number}
            res.writeHead(200);
            res.end({

            });
            break;
        case "/song": // ?id={number}[,{number}]?
            res.writeHead(200);
            res.end({

            });
            break;
        case "/artist": // ?id={number}[,{number}]?
            res.writeHead(200);
            res.end({

            });
            break;
        default:
            if (mimeTypes[extname(req.url)])
                res.writeHead(200, { 'Content-Type': mimeTypes[extname(req.url)] });
            else
                res.writeHead(200)
            const path = `.${req.url}`
            if (existsSync(path))
                res.write(readFileSync(path));
            else
                res.write('')
            res.end();
    }
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});